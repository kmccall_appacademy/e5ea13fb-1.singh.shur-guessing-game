loop do
  puts 'please enter a file name'
  filename = gets.chomp
  begin
    data = File.readlines(filename)
  rescue
    puts 'unable to read file'
    next
  end
  data.shuffle!
  File.open(filename + '-shuffled.txt', "w") { |f| data.each { |s| f.puts s } }
  break
end
