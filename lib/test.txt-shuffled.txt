
I/O Exercises
The computer should choose a number between 1 and 100 and prompt the user to Guess a number:.
Hint: You could create a random number using the Random class, or you could use the shuffle method in Array.

Write file_shuffler program that: * prompts the user for a file name * reads that file * shuffles the lines * saves it to the file "{input_name}-shuffled.txt".
Get a guess from the user
Track the number of guesses the player takes
Each time through a play loop the method should:
When the player guesses the number, print out what the number was and how many guesses the player needed.
Print the number guessed and whether it was too high or too low
Write a guessing_game method.
